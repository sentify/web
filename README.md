Sentify website, developed on hugo using GitLab Pages

Spell checker by: [streetsidesoftware.code-spell-checker](https://github.com/streetsidesoftware/cspell)

Environment Variables:

variable: hugo_config_path
values: [config.yaml, config_dev.yaml, config_maint.yaml]

Use config_dev.yaml to prepare changes awaiting release.

