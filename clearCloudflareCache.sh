echo "purging cache"

curl -X POST "https://api.cloudflare.com/client/v4/zones/$1/purge_cache" \
     -H "X-Auth-Email: chris.wellington@integrationqa.com" \
     -H "X-Auth-Key: $2" \
     -H "Content-Type: application/json" \
     --data '{"purge_everything":true}'


# 3cea215a85d6e0a1d1c31fb2f6da5aee
# 16ed07595965069cee1d08a05b64e78070b6e