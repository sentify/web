This test will carry out the following validation against the iqa website using Ruby and can run in a Docker environment;
   - Verify there are no broken icons
   - Verify there are no broken links
   - Check the log and report any required errors

# Build docker image

`docker build -t registry.gitlab.com/integrationqa/sentify/web/site:tester .`

Push it to gitlab repo (_you'd need to login through docker login if haven't already_)

`docker push registry.gitlab.com/integrationqa/sentify/web/site:tester`

## Install
Have installed:

* Chrome
* ruby

`gem install bundler` Install bundler
`bundle install` - at root level to install dependencies

# Overall RSpec tests

To run tests
`bundle exec rspec`
or
`rspec`

# TODO
* Run tests for each subpages 
