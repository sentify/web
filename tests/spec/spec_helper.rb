# frozen_string_literal: true

require 'watir'
require_relative 'checker'
require 'fileutils'

# Class to contain global test information
class TestContainer
  CHROME_LOG_PATH = Dir.pwd + '/Chrome.log'
  @tested_links = []
  @internal_links = []

  WEB_SITE_URL = 3

  class << self
    # @return [String] Index page of site tested
    attr_accessor :url
    attr_accessor :browser
    attr_accessor :tested_links
    attr_accessor :internal_links
  end
end

# Start the Chrome browser with output log details to the required Log file
TestContainer.browser = Watir::Browser.new :chrome, args: ['--disable-popup-blocking', '--disable-gpu', '--no-sandbox', '--disable-dev-shm-usage', '--enable-logging', '--v', "--log-file=#{TestContainer::CHROME_LOG_PATH}"]
TestContainer.url = ENV['BASE_URL'] || 'https://integrationqa.gitlab.io/sentify/web/site'

puts("BASE_URL = #{TestContainer.url}")

TestContainer.browser.goto TestContainer.url

FileUtils.mkdir_p 'logs'
TestContainer.browser.screenshot.save 'logs/page.png'

# Record all internal links from the index page
TestContainer.browser.links.each do |link|
  href = link.href
  next if href.empty? || TestContainer.internal_links.include?(href)

  TestContainer.internal_links << href if href.start_with? TestContainer.url
end
puts "Referenced links from index are #{TestContainer.internal_links}"

at_exit do
  TestContainer.browser.quit
end
