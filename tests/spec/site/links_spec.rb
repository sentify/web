# frozen_string_literal: true

RSpec.describe 'Links on index page' do
  TestContainer.browser.goto TestContainer.url
  TestContainer.browser.links.each do |link|
    href = link.href
    next if href.empty? || TestContainer.tested_links.include?(href)

    TestContainer.tested_links << href
    it "Link #{href} is found" do
      expect(Checker.link_status(href)).to be_between 200, 299
    end
  end
end

RSpec.describe 'Links on subpages' do
  TestContainer.internal_links.each do |internal_link|
    context internal_link do
      TestContainer.browser.goto internal_link
      TestContainer.browser.links.each do |link|
        href = link.href
        next if href.empty? || TestContainer.tested_links.include?(href)

        TestContainer.tested_links << href
        it "Link #{href} is found" do
          expect(Checker.link_status(href)).to be_between 200, 299
        end
      end
    end
  end
end
