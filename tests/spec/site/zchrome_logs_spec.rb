# frozen_string_literal: true

describe 'ChromeLogs' do
  it 'Should have no errors' do
    file = File.readlines(TestContainer::CHROME_LOG_PATH)
    # Keep all error lines except those being bypassed due to known issues with the Chrome Browser used in a pipeline
    error_lines = file.keep_if { |line| line.include?('ERROR') && !line.include?('browser_switcher_service') && !line.include?('edid_parser.cc(102)') && !line.include?('ERROR:bus.cc(393)') }
    expect(error_lines).to be_empty
  end
end
