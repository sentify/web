# frozen_string_literal: true

RSpec.describe 'Icons on page' do
  TestContainer.browser.goto TestContainer.url
  TestContainer.browser.imgs.each do |image|
    src = image.src
    next if src.empty? || TestContainer.tested_links.include?(src)

    TestContainer.tested_links << src
    it "Image #{src} is found" do
      expect(Checker.link_status(src)).to be_between 200, 299
    end
  end
end

RSpec.describe 'Icons on subpages' do
  TestContainer.internal_links.each do |internal_link|
    context internal_link do
      TestContainer.browser.goto internal_link
      TestContainer.browser.imgs.each do |image|
        src = image.src
        next if src.empty? || TestContainer.tested_links.include?(src)

        TestContainer.tested_links << src
        it "Image #{src} is found" do
          expect(Checker.link_status(src)).to be_between 200, 299
        end
      end
    end
  end
end