---
weight: 2
slider_filter:
- case-study
work:
- Strategy
- Agile
- Change

title: Agile & DevOps Discovery & Current State Assessment by Doing
description: Actions over words breaks consultant fatigue to build trust and momentum.  Allowing the University of Newcastle to embrace a new beginning.
featuredImage:
modifierClass: white-logo
clientLogo: uon.png
#metricType1: Manual Deploy
#metricValue1: 3 days
#metricType2: Auto Deploy
#metricValue2: 90 mins

section:
  - title: The Context
    description: The university’s IT leaders knew they needed to adopt Agile & DevOps and wanted to know how to go about that. They asked us to help them understand their current state, what was possible, and some steps they could take to realise that potential.

  - title: The Problem
    description: The University had consultant fatigue, staff weren't engaging.  A different approach was required where our actions would speak louder than our words. The university would only adopt the ideas of customer-focus, iteration and collaboration if they could seem them first hand.  We set out to embody Agile & DevOps values at every step.
  - title: The Value
    description: At the final showcase, the university sponsors stressed the impact of the approach. In just six weeks, they could already see people talking and acting in new ways. They recognised that adopting Agile & DevOps was a journey that they needed to take in an Agile & DevOps way. They said they had been so involved in the preparation of the report that it felt like their report to themselves.

# Table
tableTitle:
  left: What we did
  right: Incremental Value
tableContent:
  - left: Weekly showcases
    right: The team showcased an MVP of their report in their first week. They iterated the report based on feedback and showcased progress every week.
  - left: Shared models, collateral and tools
    right: The team shared the Agile & DevOps framework, their Governance model for COTS and SaaS and automation exemplars.
  - left: Collaborative documentation
    right: The team edited all interview and workshop notes in collaboration with participants. They outlined their report visibly on a wall.
  - left: Started a centre of excellence
    right: By linking the Agile & DevOps enthusiasts with each other and outside communities, the team started a new network and knowledge-sharing.
  - left: Drop-in conversations
    right: The team kept their door open for ad hoc conversations about how Agile & DevOps could work for the client.
  - left: Mini-workshops
    right: In workshops like Governance and delivery have a friendly chat, the team involved the client in new agreements for iterative delivery of value.
  - left: Innovation incubator
    right: Around 25 people formed teams to pitch for executive support to work on four-week experiments to Agile & DevOps ways of working.
---
