---
weight: 1
publishDate: 04 Mar 2021
slider_filter:
- case-study
work:
- Culture
- Agile
- Change

title: The Circuit Breaker That Initiated the Transformation Journey
description: Twelve months into an ICT transformation program, little had changed in their organisation.  There were bottlenecks at every turn.  Every major area of the company including Governance, Architecture, Infrastructure, Change Management, Security, and Leadership were experiencing problems.
featuredImage:
clientLogo: aus-fed-logo.png
modifierClass: black-logo
section:
  - title: The Problem
    description: Our client engaged Sentify to provide a variety of coaching services.  These included agile coaching, leadership coaching, technical coaching, and governance advice.  As a consultancy we were in the unique position of being able to see the organisation as a whole.  It transpired that there were significant trust issues between teams, management and individuals.  A 10-day circuit breaker changed all that.
  - title: The Intervention
    description: Sentify took the opportunity to pull together a group of well-respected staff from each of the delivery disciplines.  We arranged to co-locate them for two weeks with the sole focus of showcasing an automated process of build and release back to senior management.
  - title: The Value
    description: At the end of the two weeks, the team had proven to themselves that they could work differently and show visible value.  It was powerful message to bring back to the company and to leadership.  A change was clearly possible and all it took was a partnering of the right people and formulating a plan in context for the organisation.  This initial experiment became the catalyst to onboard 2000 developers onto the first Enterprise adopted CI-CD platform.

# testimonial
testimonial:
  by: Chris Wellington
  designation: CEO & Founder, Sentify
  avatar:
  text: Despite people feeling very uncomfortable and expressing thoughts that it would fail, the intervention by Sentify has resulted in creating a new momentum and catalyst for successful change.

# Table
#tableTitle:
#  left: 
#  right: 
#tableContent:
#  - left: 
#    right: 
---
