---
weight: 2
slider_filter:
- case-study
work:
- Governance
- Agile

title: Combining Customer Value Mapping and Risk for Safer Delivery​
description: While a cross-programme workshop seemed unnecessary overhead, the opposite was proven. “Now we cannot see how such a programme could be successful without one. We aligned teams, and galvanised improvement priorities with a renewed energy. It was altogether a very worthwhile exercise."

featuredImage:
clientLogo: qhealth.png
modifierClass: colour-logo
section:
  - title: The Context
    description: Like many Government organisations, Queensland Health has a great number of talented, caring people and is determined to give them the best opportunities to excel. This can be challenging with complex technology, changing legislation and multiple vendors. But a recent workshop helped a QHealth team understand its delivery chain in a way not experienced before.
  - title: The Work
    description: The workshop began with a collaboration to co-design a path forward the outcomes we wanted from the workshop. Being aligned from the beginning meant that agreeing action and priorities was frictionless. From the outset the team uncovered hidden assumptions about how the organisation worked and how different areas within the programme operated. Each one was mapped and clarified.​  The group gradually established a map of the programme’s delivery methodology from insight to delivery across the entire Value Stream which afforded insight into risks and constraints. 
  - title: The Value
    description: “We revealed unknown risks not just to the programme schedule but to the quality of the products. We developed coping strategies, and adjusted delivery processes to reduce risk likelihood and impact.” Most importantly the appreciation of joint goals allowed ego and politics to remain outside. The process created trust such that people could speak freely. The entire programme was confident to bring forward good ideas and solve raised issues in good faith.​

# testimonial
#testimonial:
#  by: 
#  designation: 
#  avatar:
#  text: 

# Table
#tableTitle:
#  left: 
#  right: 
#tableContent:
#  - left: 
#    right: 
--- 