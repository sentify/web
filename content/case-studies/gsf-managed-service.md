---
weight: 1
slider_filter:
- case-study
work:
- Governance
- Cloud
- Managed Service
title: Restoring Confidence In ICT Delivery.
description: The NZ Government Superannuation Fund was looking for a substantial improvement in its ICT operational and support service levels.  With our help they were able to move to a new service delivery construct that included a regular cadence of feature development, strong IT governance, technical guardianship, and robust support.

featuredImage:
clientLogo: nzgsfa.png
modifierClass: white-logo
section:
  - title: The Client
    description: The Government Superannuation Fund (GSF) was established in 1948.  Its purpose is to provide retirement benefits for government and other state sector employees.  Their responsibilities include distributing over $800m to members annually and they reached out to Sentify for assistance in uplifting service levels.  Sentify secured the managed services contract in 2019.
  - title: The Outcome
    description: Twelve months after working with Sentify, GSF has a new faith in ICT delivery and service.  System enhancements are a regular activity, balanced across business features, technical debt, and IT resilience.  Sentify now provides GSF with long term strategic guardianship.  By collaborating closely with our clients and working transparently we are able to open dialogue to identify and resolve problems that cause consistent frustration and develop new ways of interacting.  This will allow us to continue serving them well into the future.

# testimonial
#testimonial:
#  by: Simon Tyler
#  designation: Chief Executive
#  avatar:
#  text: Managing our core IT system was painful, we lacked transparency and making a change just seemed hard.  Sentify delivered on what they proposed, we work with a partner we trust and move forward with confidence in our system.

# Table
#tableTitle:
#  left: 
#  right: 
#tableContent:
#  - left: 
#    right: 
---
