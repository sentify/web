---
title: About Us
descriptionone: Sentify (previously IntegrationQA) was founded by Chris Wellington in 2007. Starting out in Wellington NZ, our initial success was in helping organisations shift quality left through an automated approach to assurance. The team has grown and evolved significantly since that time. Our focus is still the same, which is to help drive, facilitate and implement improvements to make organisations better.    
descriptiontwo: Today we support a global market with staff in New Zealand, Australia, United States and Israel.  We’ve seen digital collaboration break down boarders and open new markets to customers that seek specialist skills and experience dealing with cultural, process and technology change in highly regulated and complex environments.
descriptionthree: At the heart of Sentify are our people. We can best be described as a diverse bunch of smart, passionate and innovative folk who enjoy working together to make a real difference to you and your business. We embrace our diversity and it is our diversity in thinking, skills and experience that enable us provide unique approaches to how we can engage and assist you.
ourmission: 
  title: Our Mission
  description: Enable organisations to achieve better business outcomes through improving the way they work

ourvalues:
  title: Our Values
  description: Back in 2016 we sat down as a company and captured the values we felt best represented how we worked and acted.  Today we still see these as an essential set of attributes to guide us as we continue to grow.
  list:
    - title: Deliver value for staff and customers
    - title: Innovate, adapt and improve
    - title: Walk the talk, recommend and implement
    - title: Be honest and transparent


gettingstarted:
  title: Getting to know us
  description: If you have reached this far, why not take the next step. We like to be transparent in our methods and approach and find visualising a process helps gain understanding and provide clarity.  Here is a 4 step example of what to expect with engaging with us.
  list:
    - title: Ice Breaker
      image: ice-breaker.svg
      description: Let's get to know each other.
      step: Is there a rapport and common understanding?
    - title: Drivers
      image: drivers.svg
      description: Let's clarify the outcomes you seek, what are you worried about, and how should we measure success?
      step: Do our values align?
    - title: Alignment
      image: alignment.svg
      description: Let's get together to bounce off ideas to come up with a set of prioritised actions.
      step: Are we on the same page?
    - title: Action
      image: action.svg
      description: Let's write down what each of us will do and how we will succeed together.
      step: Let's co-own the outcome.


---
