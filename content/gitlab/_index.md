---
title: GitLab
description: Sentify is a GitLab Select and Managed Services Partner, supporting organisations and teams to realise return on their GitLab investment through tooling implementation, feature adoption, tool consolidation, team strategy and training as well as advisory.  You can turn to us for fast, responsive and practical advice.
graphicClass: blob-graphic
graphicImage1: gitlab
graphicImage2: blob-purple-gitlab

attributes:
  - title: GitLab Implementation
    icon: delivery-excellence
    colour: blue
    description: Seek assistance and specialist support to install, upgrade, and maintain your GitLab instance. Data Sovereignty stopping you from going to the Cloud? We've got you covered in Australia and New Zealand.
  - title: Procure
    icon: training-icon
    colour: orange
    description: Purchase GitLab through us, to enjoy responsive renewals under a local commercial arrangement, tooling health checks and workshops as well as local support services  - at no extra cost to you.
  - title: Feature Optimisation
    icon: managed-service-icon
    colour: orange
    description: Unleash the full potential of the DevSecOps features available in GitLab. We provide best practice guidance and implementation from integrating GitLab with other tools, establishing effective reporting all the way to unlocking CI/CD, Kubernetes and DAST.

brandLogos:
  - item: apnic-logo
  - item: qld-gov-logo
  - item: deakin-logo
  - item: sa-gov-logo
  - item: cquniversity-logo
  - item: vic-gov-logo
  - item: heritage-logo
  - item: aus-fed-logo
  - item: aarnet-logo

box_list_quote:
  section: Here to help
  title: Specialist Services
  quote: '"quote of some type"'
  author: abc, xyz
  list:
    - image: rapid-discovery-co-learning.svg
      title: GitLab Features 
      description: Unleash the full potential of GitLab's full feature stack and reduce costs and time spent using multiple other tools.
    - image: troubleshoot-a-problem.svg
      title: Competitive Renewal Pricing
      description: Competitive quotes, simplified procurement for your next project or renewal.
    - image: map-workflow-set-priority.svg
      title: Governance & Architecture 
      description: Guidance on security, design, hardening, integrating release, change and CI processes.
    - image: map-workflow-set-priority.svg
      title: Team Adoption 
      description: Advising and coaching teams getting started or adopting good practices, behaviour, and standards.
    - image: showcase-what-good-looks-like.svg
      title: Sovereign Hosting & Managed Services
      description: Secure, scalable GitLab stack hosted in our sovereign environment or your Cloud with seamless management and maintenance. 
    - image: showcase-what-good-looks-like.svg
      title: Superior Technical Support
      description: Dedicated and responsive support when you need it.  


box_panel:
  section: Here to help
  title: Specialist Services
  description: 
  boxes:
    - column: 
      image1: a-runner.png
      title1: GitLab Features 
      description1: Unleash the full potential of GitLab's full feature stack and reduce costs and time spent using multiple other tools.
      image2: lb-bug.png
      title2: Competitive Renewal Pricing
      description2: Competitive quotes, simplified procurement for your next project or renewal.
    - column:
      image1: lb-pie.png
      title1: Governance & Architecture
      description1: Guidance on security, design, hardening, integrating release, change and CI processes.
      image2: a-three-person-stars.png
      title2: Team Adoption & Training 
      description2: Advising and coaching teams getting started or adopting good practices, behaviour, and standards.
    - column:
      image1: a-letterbox-cloud.png 
      title1: Sovereign Hosting & Managed Services
      description1: Secure, scalable GitLab stack hosted in our sovereign environment or your Cloud with seamless management and maintenance. 
      image2: lb-point-idea.png
      title2: Superior Technical Support
      description2: Dedicated and responsive support when you need it.
---
