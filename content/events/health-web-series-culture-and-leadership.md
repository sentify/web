---
weight: 3
#slider_filter:
#- case-study
work:
- Culture
- Leadership

title: ORGANISATIONAL CULTURE - going from good to great without the guesswork
heading: 

section:
  - title: Who is this for
    description: Any leader, manager or team member curious to create a great workplace culture to help drive quality outcomes.
  - title: What will be presented
    description: Join special guest Rebecca Lorains, CEO of Primary Care Connect Shepparton, with Ric Taylor, Sentify's Practice Lead for Culture and Leadership, as they present an exemplar case study of evidence-based organisational culture development at Primary Care Connect in Shepparton.
  - title: Key outcomes for participants
    description: Appreciate an internationally evidence-based approach to assessing the complexity of organisational culture and leadership. 
  - title: 
    description: Explore tangible metrics and benefits of investing in management and leadership development utilising high quality 360 feedback and coaching.  
  - title: 
    description: See how a multi-faceted and responsive program which engaged with the whole organisation supported growth, mitigated the challenges of the pandemic and impacted positively on patient/client experience.  
  - title: 
    description: Reflect on the lived experience of culture and leadership in your own organisation, and consider your own priorities and next steps.
  - title: When
    description: Thursday 22nd April 11am AEST, 1pm NZT. Register below, to join this session.

background:
  title: Further background about this case study
  summary: This session dives into the culture and leadership journey of Primary Care Connect (PCC), a community health service in regional Victoria. Hearing from both the CEO of PCC and the consulting partner on this 18-month journey will provide participants with fascinating and useful insights from an internal and external perspective.
  description: Organisational culture is by nature complex, however there are very clear correlations between highly constructive cultures, staff experience and patient/client outcomes. The evidence-based approach used here for culture and leadership assessments enabled a critical clarity and a confidence to focus on the right causal factors. In turn the various intersecting elements generated significant results within the organisation, positively impacting the following.
  list:
    - title: Strategic planning
    - title: Governance and alignment
    - title: Mgmt & leadership capability & impact
    - title: Staff engagement
    - title: Service delivery
    - title: Patient experience.
  final: There were considerable challenges along the way and many opportunities for further growth. The many human factors which make culture and leadership so dynamic will be explored and future directions for PCC highlighted.

---

