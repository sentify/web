---
title: Parasoft
description: Sentify is the Australasian distributor for Parasoft, providing resellers and customers a single point of contact for licensing and support.
graphicClass: blob-graphic
graphicImage1: parasoft
graphicImage2: blob-blue-parasoft

attributes:
  - title: Resell
    icon: training-icon
    colour: orange
    description: Interested in becoming a reseller, we would be happy to guide you through the process.
  - title: Procure
    icon: delivery-excellence
    colour: blue
    description: Need a quotation, ready to buy or just looking for more information?
  - title: Support
    icon: managed-service-icon
    colour: orange
    description: Need technical help, access to upgrades or downloads?

brandLogos:
  - item: qrail
  - item: nab-logo
  - item: cua-logo
  - item: eos
  - item: vodafone-logo
  - item: boeing-logo



---
