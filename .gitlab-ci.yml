# All available Hugo versions are listed here: https://gitlab.com/pages/hugo/container_registry
stages:
  - lint
  - build
  - test
  - pages
  - lighthouse
  - cloudflare

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  # for debug only
  CI_DEBUG_TRACE: "true"
  
spell-check:
  stage: lint
  image: node:alpine
  script:
    - npm install -g cspell
    # apparently it doesn't recurse through directories...
    - cspell lint ./* ./**/* ./**/**/* ./**/**/**/*

pages:
  image: registry.gitlab.com/pages/hugo/hugo_extended:latest
  stage: pages
  script:
    - hugo --baseURL=$pages_endpoint --config $hugo_config_path
  artifacts:
    paths:
      - public
  except:
    - schedules
  only:
    - master
    - dev

build:
  image: registry.gitlab.com/pages/hugo/hugo_extended:latest
  stage: build
  script:
    - hugo --config config.yaml --baseUrl http://localhost:4000/ --destination build
  except:
    - master
  artifacts:
    paths:
      - build

test:
  image: registry.gitlab.com/integrationqa/sentify/web/site:tester
  stage: test
  variables:
    BASE_URL: http://localhost:4000/
  script:
    - /test-site/bin/jekyll serve -s build -B
    - cd tests
    - xvfb-run --server-args="-screen 0, 1280x1024x24" bundle exec rspec
  artifacts:
    paths:
      - ./tests/logs
    reports:
      junit: ./tests/logs/*.xml
    expire_in: 1 week
    when: always

docker-build-tester:
  services:
    - docker:19.03.5-dind
  image: docker:19.03.5
  stage: build
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE":tester tests
    - docker push "$CI_REGISTRY_IMAGE":tester
  except:
    - master
  only:
    refs:
      - merge_requests
    changes:
    - tests/Dockerfile

.compose_test:
  services:
    - docker:19.03.5-dind
  image: docker:19.03.5
  stage: test
  script:
    - apk add docker-compose
    - docker-compose version
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY # Required as docker image referred to is private
    - docker-compose up --abort-on-container-exit --exit-code-from test
  variables: # Currently this BASE_URL is ignored. Potentially a UAT job could test this
    BASE_URL: "https://integrationqa.gitlab.io/sentify/web/site"
  except:
    - master
  artifacts:
    paths:
      - ./tests/logs
    reports:
      junit: ./tests/logs/*.xml
    expire_in: 1 week
    when: always
  tags:
    - docker

lighthouse:
  image: markhobson/node-chrome
  stage: lighthouse
  before_script:
    # Install Lighthouse
    - npm i -g lighthouse
  script:
  # Run Lighthouse test
  - bash -c -x "./checkSiteAndSendHeartBeat.sh $pages_endpoint $genie_key"
  artifacts:
    paths:
      - ./report.html
  only:
    - schedules

cloudflare:
  stage: cloudflare
  image: markhobson/node-chrome
  script:
    - bash -c -x "./clearCloudflareCache.sh $cloudflare_zone $cloudflare_global_api_key"
  except:
    - schedules
  only:
    variables:
      - $CI_PROJECT_ID == "23701250"
